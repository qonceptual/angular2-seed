var params = browser.params;

//credit card
var cardName = element(by.model('card.name')),
  cardAddress = element(by.model('card.addressLine1')),
  cardCity = element(by.model('card.addressCity')),
  cardState = element(by.model('card.addressState')),
  cardZip = element(by.model('card.addressZip')),
  cardNum = element(by.model('card.number')),
  cardCVC = element(by.model('card.cvc')),
  cardMonth = element(by.model('card.expMonth')),
  cardYear = element(by.model('card.expYear'));

exports.convertCard = function (validity) {
  if (validity == 'valid') {
    console.log('Test has requested the use of a VALID credit card number.');
    var rndm = function getRandomInt() {
      return Math.floor(Math.random() * (14 - 1 + 1)) + 1;
    };
    var pickACard = rndm();
    console.log(pickACard);
    var validCard;

    if (pickACard == '1') {
      console.log('visa1');
      validCard = params.cc.visa1;
    }
    else if (pickACard == '2') {
      console.log('visa2');
      validCard = params.cc.visa2;
    }
    else if (pickACard == '3') {
      console.log('visaDebit');
      validCard = params.cc.visaDebit;
    }
    else if (pickACard == '4') {
      console.log('mc');
      validCard = params.cc.mc;
    }
    else if (pickACard == '5') {
      console.log('mcDebit');
      validCard = params.cc.mcDebit;
    }
    else if (pickACard == '6') {
      console.log('mcPrePaid');
      validCard = params.cc.mcPrePaid;
    }
    else if (pickACard == '7') {
      console.log('amEx1');
      validCard = params.cc.amEx1;
    }
    else if (pickACard == '8') {
      console.log('amEx2');
      validCard = params.cc.amEx2;
    }
    else if (pickACard == '9') {
      console.log('discover1');
      validCard = params.cc.discover1;
    }
    else if (pickACard == '10') {
      console.log('discover2');
      validCard = params.cc.discover2;
    }
    else if (pickACard == '11') {
      console.log('dinersClub1');
      validCard = params.cc.dinersClub1;
    }
    else if (pickACard == '12') {
      console.log('dinersClub2');
      validCard = params.cc.dinersClub2;
    }
    else if (pickACard == '13') {
      console.log('jcb1');
      validCard = params.cc.jcb1;
    }
    else if (pickACard == '14') {
      console.log('jcb2');
      validCard = params.cc.jcb2;
    }
    else {
      fail('The value "' + pickACard + '" does not match any credit cards in the convertCard() random card selector.');
    }

    return {validCard: validCard, pickACard: pickACard};
  }
  else if (validity == 'invalid') {
    console.log('Test has requested the use of an INVALID credit card number.');
    var rndm = function getRandomInt() {
      return Math.floor(Math.random() * (5 - 1 + 1)) + 1;
    };
    var pickACard = rndm();
    console.log(pickACard);
    var invalidCard;

    if (pickACard == '1') {
      console.log('declined');
      invalidCard = params.ccError.declined;
    }
    else if (pickACard == '2') {
      console.log('declinedFraud');
      invalidCard = params.ccError.declinedFraud;
    }
    else if (pickACard == '3') {
      console.log('incorrectCVC');
      invalidCard = params.ccError.incorrectCVC;
    }
    else if (pickACard == '4') {
      console.log('expCard');
      invalidCard = params.ccError.expCard;
    }
    else if (pickACard == '5') {
      console.log('processingError');
      invalidCard = params.ccError.processingError;
    }
    else {
      fail('The value "' + pickACard + '" does not match any credit cards in the convertCard() random card selector.');
    }

    return {invalidCard: invalidCard, pickACard: pickACard};
  }
  else {
    console.log('Please specify either a VALID or INVALID credit card number for the test.');
  }
};

exports.card = function (UID, type) {
  //credit card
  //expect(page.continueBtn.getAttribute('disabled')).toBeTruthy();
  cardName.sendKeys(UID);
  //expect(page.continueBtn.getAttribute('disabled')).toBeTruthy();
  cardAddress.sendKeys(UID + ' St.');
  //expect(page.continueBtn.getAttribute('disabled')).toBeTruthy();
  cardCity.sendKeys('Mt. Pleasant');
  //expect(page.continueBtn.getAttribute('disabled')).toBeTruthy();
  cardState.sendKeys('SC');
  //expect(page.continueBtn.getAttribute('disabled')).toBeTruthy();
  cardZip.sendKeys('29492');
  //expect(page.continueBtn.getAttribute('disabled')).toBeTruthy();
  cardNum.sendKeys(type);
  //expect(page.continueBtn.getAttribute('disabled')).toBeTruthy();
  cardCVC.sendKeys('123');
  //expect(page.continueBtn.getAttribute('disabled')).toBeTruthy();
  cardMonth.sendKeys('12');
  //expect(page.continueBtn.getAttribute('disabled')).toBeTruthy();
  cardYear.sendKeys('20');
  //expect(page.continueBtn.getAttribute('disabled')).toBeFalsy();
};