function uid() {
  var currentDateTime = new Date().getTime();
  var rndmNum = Math.floor((Math.random() * 100) + 1);
  var uid = currentDateTime + rndmNum;

  return uid;
}

module.exports = uid;
