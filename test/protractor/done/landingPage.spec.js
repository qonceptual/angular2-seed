var util = require('util');

describe('Landing page', function () {
  var UID = uid();
  var params = browser.params;

  beforeEach(function () {
  });

  afterEach(function () {
  });

  it('should have gone to Google', function () {
    expect(browser.getCurrentUrl()).toContain('google.com');
  });
});
