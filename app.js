(function() {
  var AppComponent = ng
    .Component({
      selector: 'my-app',
      templateUrl: './views/landingPage.html'
    })
    .Class({
      constructor: function () { }
    });
  document.addEventListener('DOMContentLoaded', function() {
    ng.bootstrap(AppComponent);
  });
})();