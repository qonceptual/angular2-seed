Seed project for using AngularJS 2.x with Protractor 3.x and Jasmine 2.

WARNING: Angular 2.x is still in Alpha and may have issues.

Run 'npm install' to install all required dependencies for the app.
Run 'npm start' to kick up "live-server" and load the app in your default browser. It refreshes the page as you make changes to the application.