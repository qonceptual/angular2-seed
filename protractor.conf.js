//From the terminal in your root directory
//1) $ npm install
//2) $ npm test

var path = require('path');
var HtmlScreenshotReporter = require('protractor-jasmine2-screenshot-reporter');

exports.config = {
  //Please note that if you set seleniumAddress, the settings for seleniumServerJar, seleniumPort, seleniumArgs, browserstackUser, browserstackKey, sauceUser and sauceKey will be ignored.
  //The address of a running selenium server.
  //seleniumAddress: 'http://localhost:4444/wd/hub',
  seleniumServerJar: "node_modules/protractor/selenium/selenium-server-standalone-2.48.2.jar",
  seleniumPort: '4444',

  //directConnect: true,

  baseUrl: 'http://google.com',

  //Spec patterns are relative to the location of the spec file. They may include glob patterns.
  suites: {
    done: 'test/protractor/done/*spec.js',
    stripe: 'test/protractor/stripe/*spec.js',
    inDev: 'test/protractor/inDev/*spec.js',
    smoketest: 'test/protractor/done/landingPage.spec.js'
  },

  //Spec patterns are relative to the current working directly when protractor is called.
  specs: ['test/protractor/done/*.spec.js'],

  framework: 'jasmine2',
  //framework: 'cucumber',

  //Options to be passed to Jasmine-node.
  jasmineNodeOpts: {
    //If true, print colors to the terminal.
    showColors: true,
    //Default time to wait in ms before a test fails.
    defaultTimeoutInterval: 99999
    //Function called to print jasmine results.
    //, print: function() {}
    //If set, only execute specs whose names match the pattern, which is
    //internally compiled to a RegExp.
    //grep: 'pattern',
    //Inverts 'grep' matches
    //invertGrep: false
  },

  allScriptsTimeout: 99999,

  chromeDriver: './node_modules/protractor/selenium/chromedriver',

  //Capabilities to be passed to the webdriver instance.
  capabilities: {
    browserName: 'chrome'
    //, shardTestFiles: true
    //, maxInstances: '3'
  },

  //multiCapabilities: [{
  //   browserName: 'firefox',
  //   shardTestFiles: true,
  //   maxInstances: '2'
  //}, {
  //   browserName: 'chrome',
  //   shardTestFiles: true,
  //   maxInstances: '2'
  //}],

  params: {
    login: {
      personal: 'qa.testing@qonceptual.com',
      password: 'soccer'
    },
    cc: {
      visa1: '4242424242424242',
      visa2: '4012888888881881',
      visaDebit: '4000056655665556',
      mc: '5555555555554444',
      mcDebit: '5200828282828210',
      mcPrePaid: '5105105105105100',
      amEx1: '378282246310005',
      amEx2: '371449635398431',
      discover1: '6011111111111117',
      discover2: '6011000990139424',
      dinersClub1: '30569309025904',
      dinersClub2: '38520000023237',
      jcb1: '3530111333300000',
      jcb2: '3566002020360505'
    },
    ccError: {
      declined: '4000000000000002',
      declinedFraud: '4100000000000019',
      incorrectCVC: '4000000000000127',
      expCard: '4000000000000069',
      processingError: '4000000000000119'
    },
    ccErrorMessage: {
      declined: 'The card was declined',
      declinedFraud: 'The card was declined',
      incorrectCVC: 'security code is incorrect',
      expCard: 'The card has expired',
      processingError: 'An error occurred while processing the card'
    }
  },

  //sets the test window size
  onPrepare: function() {
    browser.driver.manage().window().setSize(1366,768);

    var EC = protractor.ExpectedConditions;

    global.base = exports.config.baseUrl;
    browser.get(base);

    //loading mock modules on an Angular2 app is not yet supported
    /*//Disable Ng Animations
    var disableNgAnimate = function() {
      angular.module('disableNgAnimate', []).run(['$animate', function($animate) {
        $animate.enabled(false);
      }]);
    };
    browser.addMockModule('disableNgAnimate', disableNgAnimate);

    //Disable CSS Animations
    var disableCssAnimate = function () {
      angular.module('disableCssAnimate', []).run(function () {
        var style = document.createElement('style');
        style.type = 'text/css';
        style.innerHTML = '* {' +
          '-webkit-animation: none !important;' +
          '-moz-animation: none !important;' +
          '-o-animation: none !important;' +
          '-ms-animation: none !important;' +
          'animation: none !important;' +
          '}';
        document.getElementsByTagName('head')[0].appendChild(style);
      });
    };
    browser.addMockModule('disableCssAnimate', disableCssAnimate);*/

    jasmine.getEnv().addReporter(
      new HtmlScreenshotReporter({
        dest: 'test/protractor/reports',
        filename: 'protractor_report.html',
        ignoreSkippedSpecs: true,
        captureOnlyFailedSpecs: true,
        reportOnlyFailedSpecs: false
        //, pathBuilder: function(currentSpec, suites, browserCapabilities) {
        //will return chrome/your-spec-name.png
        //return browserCapabilities.get('browserName') + '/' + currentSpec.fullName;
        //}
      })
    );

    var SpecReporter = require('jasmine-spec-reporter');
    //add jasmine spec reporter
    jasmine.getEnv().addReporter(new SpecReporter({displayStacktrace: true}));

    //helpers
    global.common = require('./test/protractor/helper/commonObjects.js');
    global.uid = require('./test/protractor/helper/uid.js');
    global.date = require('./test/protractor/helper/findDate.js');
    global.billing = require('./test/protractor/helper/billing.js');
  },

  onComplete: function() {}

  ,plugins: [
    {
      chromeA11YDevTools: {
        treatWarningsAsFailures: false
      },
      package: 'protractor-accessibility-plugin'
    }

    ,{
      package: 'protractor-ng-hint-plugin',
      asTests: false
      //   excludeURLs: {(String|RegExp)[]}
    }

    ,{
      package: 'protractor-timeline-plugin',

      //Output json and html will go in this folder.
      outdir: 'test/protractor/timelines'

      //Optional - if sauceUser and sauceKey are specified, logs from
      //SauceLabs will also be parsed after test invocation.
      //sauceUser: 'Jane',
      //sauceKey: 'abcdefg'
    }

    ,{
     package: 'protractor-console-plugin'
      //failOnWarning: {Boolean}                (Default - false),
      //failOnError: {Boolean}                  (Default - true),
      //logWarnings: {Boolean}                  (Default - true),
      //exclude: {Array of strings and regex}   (Default - [])
    }

    ,{
      package: 'protractor-testability-plugin'
    }

    //,{
    // package: 'protractor-console'
    // , logLevels: ['severe']
    //}
  ]
};
